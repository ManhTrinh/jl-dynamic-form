import { Component, OnInit } from '@angular/core';
import * as Survey from 'survey-angular';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app works!';
  json = {
    title: 'Create your Joblogic Survey',
    showProgressBar: 'top'
  };

  ngOnInit(): void {
    // Survey.defaultBootstrapCss.navigationButton = 'btn btn-green';
    // Survey.defaultBootstrapCss.rating.item = 'btn btn-default my-rating';
    // Survey.Survey.cssType = 'bootstrap';
  }

  onSurveySaved(survey) {
    this.json = survey;
  }
}
