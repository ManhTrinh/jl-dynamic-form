import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import * as SurveyKo from 'survey-knockout';
import * as SurveyEditor from 'surveyjs-editor';
import * as widgets from 'surveyjs-widgets';

import 'inputmask/dist/inputmask/phone-codes/phone.js';

widgets.icheck(SurveyKo);
widgets.select2(SurveyKo);
widgets.imagepicker(SurveyKo);
widgets.inputmask(SurveyKo);
widgets.jquerybarrating(SurveyKo);
widgets.jqueryuidatepicker(SurveyKo);
widgets.nouislider(SurveyKo);
widgets.select2tagbox(SurveyKo);
widgets.signaturepad(SurveyKo);
widgets.sortablejs(SurveyKo);
widgets.ckeditor(SurveyKo);
widgets.autocomplete(SurveyKo);
widgets.bootstrapslider(SurveyKo);

@Component({
  selector: 'app-survey-editor',
  template: `<div id='surveyEditorContainer'></div>`
})
export class SurveyEditorComponent implements OnInit {
  editor: SurveyEditor.SurveyEditor;
  @Input() json: any;
  @Output() surveySaved: EventEmitter<Object> = new EventEmitter();

  ngOnInit() {
    const editorOptions = { showEmbededSurveyTab: true, generateValidJSON: true };
    this.editor = new SurveyEditor.SurveyEditor(
      'surveyEditorContainer',
      editorOptions
    );
    this.editor.text = JSON.stringify(this.json);
    this.editor.saveSurveyFunc = this.saveMySurvey;

    this.editor.toolbox.addItems(
       [
        {
            iconName: 'icon-default',
            isCopied: false,
            name: 'job-asset',
            title: 'Add Job Asset',
            json: {
                type: 'dropdown',
                choicesOrder: 'asc',
                name: 'my-job-assets',
                title: 'Job Assets',
                renderAs: 'select2',
                choicesByUrl: {
                    url: 'https://jsonplaceholder.typicode.com/todos',
                    valueName: 'id',
                    titleName: 'title'
                }
            }
        },
        {
            iconName: 'icon-default',
            isCopied: false,
            name: 'site-asset',
            title: 'Add Site Asset',
            json: {
                type: 'dropdown',
                choicesOrder: 'asc',
                name: 'My Site Assets',
                title: 'Site Assets',
                renderAs: 'select2',
                choices: [
                    {
                    value: 'sa1',
                    text: 'Site Asset no1'
                    },
                    {
                    value: 'sa2',
                    text: 'Site Asset no2'
                    },
                    {
                    value: 'ja3',
                    text: 'Site Asset no3'
                    }
                ],
                questionbase: {
                    properties: [
                      {
                        name: '',
                        title: ''
                      },
                      {
                        name: 'isRequired',
                        category: 'checks'
                      }
                    ]
                }
            }
        },
        {
            iconName: 'icon-default',
            isCopied: false,
            name: 'parts',
            title: 'Parts',
            json: {
                type: 'dropdown',
                choicesOrder: 'asc',
                name: 'My Parts',
                title: 'Parts',
                renderAs: 'select2',
                choices: [
                    {
                    value: 'pa1',
                    text: 'Part no1'
                    },
                    {
                    value: 'pa2',
                    text: 'Part no2'
                    },
                    {
                    value: 'pa3',
                    text: 'Part no3'
                    }
                ]
            }
        },
        {
            iconName: 'icon-default',
            isCopied: false,
            name: 'job-parts',
            title: 'Job Parts',
            json: {
                type: 'dropdown',
                choicesOrder: 'asc',
                name: 'My Job Parts',
                title: 'Job Parts',
                renderAs: 'select2',
                choices: [
                    {
                    value: 'jp1',
                    text: 'Job Part no1'
                    },
                    {
                    value: 'jp2',
                    text: 'Job Part no2'
                    },
                    {
                    value: 'jp3',
                    text: 'Job Part no3'
                    }
                ]
            }
        }
       ]
    );

    this.editor.onCanShowProperty.add(function(sender, options){
        const customNewSelects: string[] = ['Job Assets', 'Site Assets', 'Parts', 'Job Parts'];
        if (options.obj.getType() === 'dropdown' && customNewSelects.includes(options.obj.title)) {
            options.canShow = options.property.name === 'isRequired';
        }
    });
  }

  saveMySurvey = () => {
    console.log(JSON.stringify(this.editor.text));
    this.surveySaved.emit(JSON.parse(this.editor.text));
  }
}
